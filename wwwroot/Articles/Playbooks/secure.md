---
Title: Lock Down Your Accounts
Synopsis: The first step in dealing with any privacy or security incident
Author: Jonathan Daigle
Modified: 2024-09-21
---

# Lock Down Your Accounts

So something has happened. You suspect someone has gained access to one of your accounts, or you've become the target of unwanted cyber attention. The first step is to prevent unauthorized access to your accounts. If you're being harassed, unwanted photos are being posted online, or your identity is stolen there's a good chance that this started with someone breaking into one of your accounts to begin with.

Change all passwords using a password manager, enable multi-factor on all accounts that support it, and update your privacy options on all social media sites to be as strict as possible.

## Resources

- [Securing your accounts](..\Security\securing_accounts.md) - Our article on how to improve account security
- [The Smart Girl's Guide to Privacy](https://archive.org/details/smartgirlsguidet0000blue) - An excellent book that takes a measured approach to privacy and security, targets girls and young women. The advice is useful for people of all genders and ages. Available at libraries and bookstores.
- [Get Cyber Safe](https://www.getcybersafe.gc.ca) - A set of accessible (if basic) resources provided by the Government of Canada. If you're finding a lot of theses guides go above your head, this is a great place to start.
- [Consumer Reports Security Planner](https://securityplanner.consumerreports.org) - An excellent and easy to follow guide to improve your personal security posture
- [Stay Safe Online (NCA)](https://staysafeonline.org/) - National Cybersecurity Alliance (A U.S. non-profit) has a lot of guides for handling various cyber-security situations
- [BC Society of Transition Houses Security Toolkit](https://bcsth.ca/techsafetytoolkit/) - Detailed guides for securing your digital life
- [IntelTechniques](https://inteltechniques.com/) - A security and privacy company, their book _Extreme Privacy_ is well regarded
- [The White Hatter](https://thewhitehatter.ca) - A Canadian company that provides cyber safety education and resources
- [Lock Down your life](https://www.lockdownyourlife.com/) - A company that provides classes and consultation services regarding cyber safety
