---
Title: Cyber Stalking, Harassment, or Bullying Playbook
Synopsis: A basic playbook of what to do if you or a loved one is a victim of cyber stalking, cyber harassment, or cyber bullying
Author: Jonathan Daigle
Modified: 2024-09-21
---

# Cyber Stalking, Harassment, or Bullying Playbook

[TOC]

This is a basic playbook on what to do if you're being stalked and harassed online.

## Ensure Your Safety

Nothing is more important.

### Physical Security

Especially in cases of stalking or dealing with harassment from people who know you in person, if you are facing a stalker or harasser you are at increased risk of physical violence.

There are tools and techniques to reduce this risk.

#### Avoid Being Tracked

Airtags and Tiles have made it cheap and easy to track the physical location of your own valuables, they are great attached to luggage, bikes, and keys. Unfortunately they can also be used maliciously, if an airtag is discreetly placed on your person or stuck to the underside of your vehicle it can be used to track your movements.

Airguard, available for Android ([Play Store](https://play.google.com/store/apps/details?id=de.seemoo.at_tracking_detection.release&hl=en-US&pli=1) | [F-Droid](https://f-droid.org/en/packages/de.seemoo.at_tracking_detection/)) and IOS ([App Store](https://apps.apple.com/us/app/airguard-tracking-protection/id1659427454)) can scan for and help you find these trackers.

IOS (Apple) phones have built-in support for Airtag scanning as well, please see Apple page for more information <https://support.apple.com/en-us/119874#unwantedtracking>

It is also important to ensure your Apple ID or Google account is secure, as your location history is accessible through these services.

#### Secure Your Home

If your harasser has or has had a key to your home, or if you have had keys go missing, consider having your locks re-keyed or replaced.

Ensure all of your exterior doors are properly hung with gaps covered. There are some videos below to give you an idea of what mistakes can be made when installing doors, and some things you can do to prevent them.

<iframe src="https://www.youtube.com/embed/byYGPO4ptxs" title="[978] Opening a Locked Door With Movie Film (With Deviant Ollam)" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>
<iframe src="https://www.youtube.com/embed/O74Q1VTz4j4" title="[979] Reaching UNDER a Door To Open It? (With Deviant Ollam)" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>
<iframe src="https://www.youtube.com/embed/bX-6LFPSQuk" title="[980] Simple, Easy, &amp; Effective: The Traveler Hook Attack (With Deviant Ollam)" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>
<iframe src="https://www.youtube.com/embed/nJu_-Iuppc0" title="[981] The Pen-Sized Hinge Pin Destroyer! (With Deviant Ollam )" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>

Ensure all windows are locked as well, if your windows don't have locks, sticks of the appropriate length can be put between the window and the edge of the frame to prevent it from opening. A screen will **not** prevent someone from coming in, screens are easily removed (even from the outside) and of course easily cut. Second story windows and balcony doors should also be locked.

Deadbolts are significantly more secure than doorknob locks, using long wood screws to attach your strike plate to the wall helps resist physical smashing. You can get tall or even full length strike plates to resist kicking, such as the product featured here: <https://youtu.be/FFUKVpaJuKg>

In Canada it is our understanding that it is not lawful to carry a weapon for the express purpose of self-defense. Items such as bear spray or air horns can offer non-lethal ways of protecting oneself, but consult a lawyer to be sure you understand your rights and restrictions.

#### Keep Trusted People Around

This is challenging for a variety of reasons, you may find your situation embarrassing, or you may feel you have alienated or have been isolated from the people you trust (people getting out of abusive relationships, for example, have often been isolated or even alienated from friends and family by their abuser).

However it is unspeakably valuable to have trusted friends and family to help you, to offer a place to stay until you feel safe, and to help manage the situation.

Don't forget your psychological safety is an important part of your safety as well. A great strategy to help maintain your psychological safety is to let someone you trust (a parent or sibling, for example) take control of the social media or communications platforms over which you're being harassed. This way they can filter the harassing messages for you, ensure that they are recorded for legal action, and help make sure only the information you need comes to you.

## Get a Lawyer(s)

You preferably want to find a lawyer who has experience with or specializes in stalking or cyber-harassment. Your lawyer can also help you explore legal recourse for addressing your situation. Even if you don't know who the bad actor is, you may be able to open a lawsuit and subpoena records from the platforms on which you were harassed in order to identify them.

## Open a Case With Law Enforcement

Ideally you want to do this as soon as possible, but beware this may be a battle. Police forces in North America (and likely globally) are notoriously inadequate at handling cyber issues. Don't be afraid to report your case to federal agencies as well, and make sure to follow up and be persistent, make it hard to be ignored.

## Find the Perpetrator

This may be something that you or your trusted people do, or something that a private investigator or your lawyer can help with. We are effectively talking about counter cyber-stalking

TODO: Complete this section, maybe remove it? I don't know.

## Show no Mercy

If you are being stalked or harassed by someone online, chances are there are or will be other victims as well. Once you reach a point where you can impose consequences, through pressing criminal charges or through a lawsuit, you do not want to stop because their family tells you "they will take care of it" or they promise "it will never happen again".

This isn't about revenge, it's about ensuring that the consequences for their actions stick, and protecting others from their abuse.

## Resources

- [CRCVC](https://crcvc.ca/) - The Canadian Resource Centre for Victims of Crime, provides links to various support resources as well as victim support help lines
- [Report a cyber crime in Canada](https://www.cyber.gc.ca/en/incident-management)
- [Report Child Sexual Abuse Material](https://protectchildren.ca/) - Provides reporting and support services for victims of (or discoverers of) CSAM
- [BC Society of Transition Houses](https://bcsth.ca/get-help-now/) - BCSTH Provides safe temporary housing and support resources for victims of domestic violence
- [Crash Override](http://www.crashoverridenetwork.com/) - Provide support and resources for victims of online abuse
- [Tech Without Violence](https://techwithoutviolence.ca/support) - A Canadian organization dedicated to eliminating gender based cyber violence
- [Coalition Against Stalkerware](https://stopstalkerware.org/) - Provide information and tools for identifying and removing stalkerware from your devices
- [Go Ask Rose](https://goaskrose.com/) - Support resources for escaping abusive relationships
- [CETA](https://ceta.tech.cornell.edu/) - Cyber safety training in resources based in New York for victims of abusive relationships
