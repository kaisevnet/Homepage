---
Title: 5. Recommended Security Software
Synopsis: Learn about what software you can use to make your computing experience safer, faster, and easier
Author: Jonathan Daigle
Modified: 2023-04-24
---

# Recommended Security Software

<!--This page is really not well suited to markdown-->

The software on this page is meant to make your computer more secure, but also to make your computer experience easier. The human element is often considered the biggest security flaw in computer systems, humans make mistakes. With the tools on this page, you can protect your computer from your mistakes.

You may notice there is no antivirus software listed on this page. There is a very simple reason for this: you have all the antivirus you need built in. If you are using Windows 10 or newer, you do not need (and should not install) any antivirus other than the built in Windows Defender. If you are using a version of Windows older than Windows 10, your top priority needs to be to upgrade, installing an antivirus will not help you.

If you are using MacOS or Linux, then your lack of antivirus software is still all you need.

The tools on this page are more effective safeguards, and unlike antivirus software, they don't slow down your computer or introduce new security issues (a common problem with antivirus programs!)

<details>
    <summary>
    <h2>For Desktops</h2>
    </summary>
    <ul class="blocklist">
    <li>
        <h3>Chocolatey</h3>
        <p>Chocolatey is a package manager for Windows. In simpler terms: Chocolatey is an app store. Installing
        programs from Chocolatey is safer than installing them from the Internet. Are you sure you have the right
        download link? Are you sure you're selecting the right options when you install? Neither of these are
        problems when you install software with Chocolatey.</p>
        <p>Chocolatey is also the best way to keep your applications up to date. Windows update will not update the
        different programs you have installed, and many programs do not update themselves. Any software you have
        installed with Chocolatey, you can update in Chocolatey with a single click.</p>
        <p>We have included a custom installer, for anyone who is not comfortable using the command line. Downloading
        and running this installer will install both Chocolatey and the Chocolatey GUI, which lets you use
        chocolatey without having to type commands into the command prompt. <strong>If you can't figure out how to
            install Chocolatey, use the installer below, it will install Chocolatey and the Chocolatey User Interface
            for you</strong>.</p>
        <a href="//chocolatey.org">Chocolatey.org</a>
        <a href="/resources/Kaisev Chocolatey Installer.exe">Kaisev Chocolatey Installer.exe (821KB)</a>
    <li>
        <h3>Unchecky</h3>
        <p>Unchecky helps prevent installing unwanted software by unchecking 'additional options' offered during the
        installation. It also offers warnings when installers are being misleading</p>
        <a href="//unchecky.com">Unchecky.com</a>
        <a href="//chocolatey.org/packages/unchecky">Chocolatey Package</a>
    </ul>
</details>
<details>
    <summary>
    <h2>For Smartphones/Tablets</h2>
    </summary>
    <ul class="blocklist">
    <li>
        <h3>Firefox for Android</h3>
        <p>If you are using an Android phone or tablet, Firefox is a must-have. We have a preference for Firefox in
        general, but on Android there really is nothing that compares. Firefox for Android supports Add-ons. In
        fact, Firefox for Android supports <em>most of the add-ons listed on this page</em>. This is an amazing
        benefit, which the other popular browsers on Android do not offer.</p>
        <a href="//play.google.com/store/apps/details?id=org.mozilla.firefox">Play Store</a>
    <li>
        <h3>Firefox Focus for IOS</h3>
        <p>The bad news is that Firefox for IOS does not, and can not, support add-ons. Apple does not allow it. The
        solution? Firefox Focus. It doesn't support add-ons, but it does have built-in ad-blocking. In addition to
        this, you can enable that same ad-blocking in Safari, check out the article linked below on
        support.mozilla.org!</p>
        <a href="//support.mozilla.org/kb/focus">support.Mozilla.org/kb/focus</a>
        <a href="//apps.apple.com/us/app/firefox-focus-privacy-browser/id1055677337">Apple Store</a>
    <li>
        <h3>BitWarden</h3>
        <p>You need to be able to access your passwords on your phone or tablet. Bitwarden has an excellent companion
        app, available on Android and IOS, to help you access your passwords securely on your mobile devices.</p>
        <a href="//play.google.com/store/apps/details?id=com.x8bit.bitwarden">Play Store</a>
        <a href="//apps.apple.com/us/app/bitwarden-password-manager/id1137397744">Apple Store</a>
    </ul>
</details>
<details>
    <summary>
    <h2>Browser Add-ons</h2>
    </summary>
    <ul class="blocklist">
    <li>
        <h3>BitWarden</h3>
        <p>BitWarden is our recommended password manager. A password manager is a service or program that stores your
        passwords, and gives you tools to help manage and use them.</p>
        <p>Re-using passwords is very dangerous. If you had used the same password for you Adobe account as your email
        account when Adobe was hacked, the hacker would now have access to your email account. It's hard to remember
        a secure password, and it's impossible to remember unique, secure passwords for every site you use. A
        password manager means you don't have to.</p>
        <p>To use Bitwarden at its best, you create one strong password for your BitWarden account, and that will be
        the only password you need to remember. Use BitWarden's password generation tool to randomly generate super
        secure passwords for all of your different sites.</p>
        <p>When used in this way, accessing your sites becomes easier, and it becomes harder for anyone else to access
        them.</p>
        <a href="//bitwarden.com">Bitwarden.com</a>
        <a href="//addons.mozilla.org/en-US/firefox/addon/bitwarden-password-manager">Firefox</a>
        <a
        href="//chrome.google.com/webstore/detail/bitwarden-free-password-m/nngceckbapebfimnlniiiahkandclblb">Chrome</a>
        <a href="//microsoftedge.microsoft.com/addons/detail/jbkfoedolllekgbhcbcoahefnbanhhlh">Edge</a>
    <li>
        <h3>uBlock Origin</h3>
        <p>You will never see the Internet the same way again. uBlock Origin is a blocker that will block
        advertisements, trackers, and known infected websites.</p>
        <p>Forget all of the 'Internet Security' and 'Safe Browsing' programs. Frankly, none of them hold a torch to a
        good ad blocker, and uBlock Origin is the best ad blocker. uBlock Origin makes your web browsing faster, more
        secure, and less cluttered.</p>
        <p>There is nothing unethical about blocking advertisements. If advertisers didn't want to be blocked, they
        should stop pushing scams and malware. That being said sometimes you want to support a website you trust,
        and allow ads on that site. This is easily done with uBlock Origin, by clicking on the add-ons icon in the
        top right of your browser, and clicking the big blue ON/OFF button.</p>
        <a href="//github.com/gorhill/uBlock">Github.com/gorhill/uBlock</a>
        <a href="//addons.mozilla.org/en-US/firefox/addon/ublock-origin">Firefox</a>
        <a href="//chrome.google.com/webstore/detail/ublock-origin/cjpalhdlnbpafiamejdnhcphjbkeiagm">Chrome</a>
        <a href="//microsoftedge.microsoft.com/addons/detail/odfafepnkmbhccpbejgmiehpchacaeak">Edge</a>
    <li>
        <h3>HTTPS Everywhere</h3>
        <p>HTTPS Everywhere is an addon that forces HTTPS. If you have read <a
            href="/Articles/Security/urls">our article on reading web addresses</a>, then you know that HTTPS
        is a connection type that prevents people from intercepting your traffic. HTTPS everywhere forces HTTPS
        connections wherever possible</p>
        <a href="//www.eff.org/https-everywhere">Eff.org/https-everywhere</a>
        <a href="//addons.mozilla.org/en-US/firefox/addon/https-everywhere">Firefox</a>
        <a href="//chrome.google.com/webstore/detail/https-everywhere/gcbommkclmclpchllfjekcdonpmejbdp">Chrome</a>
        <a href="//microsoftedge.microsoft.com/addons/detail/fchjpkplmbeeeaaogdbhjbgbknjobohb">Edge</a>
    <li>
        <h3>Privacy Badger</h3>
        <p>Privacy Badger is an add-on designed to stop people from tracking what you do on the Internet. Specifically
        advertisers will try to track what you do and where you go so they can sell that information.</p>
        <p>Privacy Badger is meant to protect your right to consent to your information being tracked across multiple
        sites.</p>
        <a href="//privacybadger.org">Privacybadger.org</a>
        <a href="//addons.mozilla.org/en-US/firefox/addon/privacy-badger17/">Firefox</a>
        <a href="//chrome.google.com/webstore/detail/privacy-badger/pkehgijcmpdhfbdbbnkijodmdjhbjlgp">Chrome</a>
        <a href="//microsoftedge.microsoft.com/addons/detail/mkejgcgkdlddbggjhhflekkondicpnop">Edge</a>
    <li>
        <h3>Decentraleyes</h3>
        <p>Decentraleyes is a clever add-on. There are certain files and resources frequently used by multiple
        websites across the Internet. Typically, your browser has to connect to a centralized server somewhere to
        download these resources any time you connect to a site that needs them. Decentraleyes does one better, it
        stores these resources on your computer. This can marginally speed up browsing, but more importantly it
        prevents the owners of the centralized servers in question to help stop them from tracking you.</p>
        <p>This add-on is not in the new Edge addon store yet. However, if you are using the newest Microsoft Edge
        click on the Chrome link instead, you can click <strong>Allow extensions from other stores</strong> at the
        top, and install the extension from the Chrome web store instead!</p>
        <a href="//Decentraleyes.org">Website</a>
        <a href="//addons.mozilla.org/en-US/firefox/addon/decentraleyes">Firefox</a>
        <a href="//chrome.google.com/webstore/detail/decentraleyes/ldpochfccmkkmhdbclfhpagapcfdljkj">Chrome</a>
    <li>
        <h3>Noscript (Advanced)</h3>
        <p>Noscript is a (regrettably) Firefox-only addon that will block any and all scripts unless otherwise
        instructed. It takes time and a bit of dedication to build up your whitelist, but it really highlights how
        abused Javascript is on the modern Internet.</p>
        <a href="//noscript.net/">noscript.net</a>
        <a href="//addons.mozilla.org/en-US/firefox/addon/noscript">Firefox</a>
    </ul>
</details>