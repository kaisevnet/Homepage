---
Title: 4. File Extensions
Synopsis: Learn how to tell the difference between file types and which types could harm your computer
Author: Jonathan Daigle
Modified: 2023-04-24
---

# Learn Your File Extensions

[TOC]

A file extension is a set of letters after a period at the end of filenames that tell your computer how to open that file. For example you can create a file named `Notes` on your computer and edit it in any simple text editor such as Notepad or Kate. But if you simply double click on such a file you will receive a prompt asking what program you would like to open it with, and it may be hard to find the program you want in the list of options. It is more convenient if your computer knows what program to open a file in, so you can add an extension to the name of the file. In this case this is a plain text file, so you can rename it `Notes.txt`. The `.txt` is the extension, and with that as part of the file name you can open the file in your default text editor by simply double clicking on it.

*Note: If you are using Windows you may not see any file extensions. This is because they are hidden by default. We will show you how to enable them later in this article.*

The file extension is the last three or four letters of a file name after the period. The file extension indicate to Windows what type of file it's dealing with. For example you will see `filename.docx` for word documents, and `filename.exe` for Windows executable files. Look at [Fileinfo's list of common file types](http://fileinfo.com/Filetypes/common) for a list of common file extensions.

The last letters following a period indicate the file extension. A file named `program.pdf.exe` is not a PDF document; it is an EXE file. This means it is an program that will run (or 'execute') when opened. Of course any file that tries to trick you with it's name like above is not to be trusted. If you do not recognize a file extension, look it up or ask someone knowledgeable if it is safe.

## Executable File Extensions

Certain files are executable. This means they are either run directly or a program reads them and runs the commands that are written within. These files are risky by nature, as it is easy to make it do anything you wan when double clicked.

<!--We are using "executable" here to mean "any file that if opened, by default will execute arbitrary actions within the file". PowerShell scripts are not included because they by default open in an editor instead of executing. I'm also choosing to focus on Windows here, when the Linux desktop really starts taking off I'll re-write this. 2024 will be the year! Right? ...right? -->

Below are some common executable file-types and their associated extensions, make sure that these files are from a trusted source before you run them.

**File-type**            | **Extension**
-------------------------|--------------
Windows Executable File  | .exe
Java Archive File        | .jar
DOS Batch File           | .bat
Visual Basic Script File | .vbs

A file being executable means it is riskier to click on. Knowing how to identify executables will help you protect yourself. If you are sent a file or download a file that is meant to be a document, a picture, or a movie, but it has one of the above extensions, it is almost certainly going to do something bad.

## Showing File Extensions

You will notice in Windows that, by default, you can't actually see your file extensions. This is because many people consider the file extensions ugly, and don't know what they do anyway. From a security perspective, this is horrible; if you can't see the file extension, you are relying on the icon to indicate the filetype. Windows Executables can use any icon they want: that file that looks like a PDF could just as easily be an EXE.

It is important then to change this setting so you can see the file extensions of all of your files. In Windows 10 this is as easy as opening **File Explorer**, clicking on **View**, then checking **File Name Extensions**.  
![A screenshot demonstrating how file extensions can be shown](/resources/FileExtensions.png)
