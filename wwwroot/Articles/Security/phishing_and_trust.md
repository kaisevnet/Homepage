---
Title: 1. When (Not) to Trust Someone
Synopsis: Learn about trust and how to protect yourself from scammers and phishers
Author: Jonathan Daigle
Modified: 2023-04-24
---

# Phishing and Trust

[TOC]

## A Suspicious Phone Call

*You receive a phone call saying that your computer has a virus, and you need to fix it. The caller says he works for Microsoft, and that he will help you for free. Can you trust him?*

Trust is an important factor in security, especially when you are seeking help. In these cases, I find it best to compare computer work to having work done on a car, consider the below example:

*You receive a phone call saying that your car has problem, and you need to fix it. The caller says he works for Dodge, and that he will help you for free.*

Immediately in the above scenario you have to wonder several things:

1. How do I know this person is from Dodge?
2. How would this person know what's wrong with my car?
3. Does Dodge even fix cars? Wouldn't that be done by a mechanic or dealership?

These same questions apply to the call about your computer.

1. You have no way of knowing who called you
2. They have no way of knowing what's going on with your computer
3. It's not Microsoft's job to fix your computer, that would be done by a computer shop.

A good rule of thumb then, is **never trust anyone who contacts *you***. If you are having computer issues, people will not approach you about it. Unless you contact someone for help, no one will know or care about your computer.

## Spotting Suspicious communications

This same approach doesn't just apply to phone calls. This applies to emails, pop-ups, and any other form of communication.

Below are some red flags to watch out for, no matter how the conversation takes place:

### Someone Contacts You

Whenever someone approaches you it should be considered suspicious, especially if you don't already have business with that person or organization.

### Someone is Trying to Pressure You

When trying to trick or scam someone putting them under pressure can cause them to make decisions against their better judgment. This can be done with threats (often including statements such as "your data will be lost" or "your account will be deleted") and by adding time pressure (such as stating something is urgent or giving a very short time-frame that an action has to be done in).

### Someone is Pushy

Even if they are polite while doing so, someone insisting that you have to do something or giving you unsolicited instructions (that is instructions you didn't ask for) is a big warning sign.

### Poor Spelling or Grammar

Legitimate professionals generally take care to communicate correctly. Poor spelling and grammar are common with attackers.

### Unexpected links or attachments

This applies specifically to emails, texts, and other written communications. **Even when you trust the sender**, if you receive an unexpected communication with a link or attachment, it may be that someone else has access to their account. When in doubt, contact the sender in person or over the phone to confirm they meant to send that attachment.

## How do you handle a possible scammer?

If something or someone contacts you, the best thing to do is contact someone you trust. If you get a popup that says you have viruses: close it and open your antivirus program through the start menu (a method you trust for opening your antivirus software and checking for viruses yourself). If a company sends you an email saying your account will be deleted: Go online, find their real phone number (obviously you can't trust the one in the email!), and ask them about it.

### Don't Panic

Attackers put pressure on you so they can influence your decisions. It is important not to get swept up in their drama. For example:

*Someone calls to tell you there is something wrong with your computer and it (or your files) will be damaged if you don't let them fix it.*

Remember that you can shut off your computer. No virus or malware can damage your computer while it is shut off.

### Don't Give Out Information

Often when scammers or phishers contact you they are trying to learn more about you so they can more effectively scam you down the road. You never want to give out information to someone you don't trust. Even if it's something that seems harmless: your name, your children's names, what kind of computer you have, what city you're in, and any other personal information can be used against you.

### Challenge the Caller

If you're still unsure, challenge the person who has contacted you. Ask them for information: the name of the company they work for, where they're located, and what their full name is. If the person claims to be from an organization, ask them a question someone from that organization should be able to answer.

*Example: Someone is contacting you saying they are from your bank. They say something is wrong with your account. Not sure if they are actually from your bank, you ask them what the account number is on that account, and they are unable to provide it.*

In the above example you know have reason to believe this call is not legitimate, and you should hang up and contact your bank using their official contact information.

### Hang Up

You do not have to reply to emails, and you don't have to entertain someone over the phone. Any reasonable person would understand if you wanted to check in with someone you trust, but a scammer will not want that to happen. They may insist that they're someone you can trust, or that you don't have time, or even that you're in legal trouble.

You can always hang up, block their number, or turn off your phone. Never put up with abuse. If someone is behaving in an abusive manner (shouting at you, for example), hang up. Do not waste politeness or etiquette on someone you believe may be scamming you.

### Contact Someone You Trust

If a caller claims to be from a company you trust, call that company's support directly by getting their phone number off of their website. You can always ask a friend, technician, or someone else you trust if they think this is safe.

## Further Reading

The Electronic Frontier Foundation's Surveillance Self-Defense page on phishing: <https://ssd.eff.org/module/how-avoid-phishing-attacks>
