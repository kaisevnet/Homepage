using Microsoft.AspNetCore.Mvc.Rendering;

namespace homepage;

public static class Extensions {
    public static string ActiveClass(this IHtmlHelper htmlHelper, string action, string controller = "Home", string cssClass = "selected")
    {
        string? currentController = htmlHelper?.ViewContext.ActionDescriptor.RouteValues["controller"]?.ToLower();
        string? currentAction = htmlHelper?.ViewContext.ActionDescriptor.RouteValues["action"]?.ToLower();
        bool matches = controller.ToLower() == currentController && action.ToLower() == currentAction;

        return matches
            ? cssClass
            : "";
    }
    public static List<string> GetFilesRecursive(string path) {
        List<string> retVal = Directory.GetFiles(path).ToList();
        foreach (var dir in Directory.GetDirectories(path)) {
            retVal.AddRange(GetFilesRecursive(dir));
        }
        return retVal;
    }
}
