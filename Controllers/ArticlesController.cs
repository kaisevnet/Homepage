using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using homepage.Models;

namespace homepage.Controllers;

public class ArticlesController : Controller
{
    private readonly ILogger<HomeController> _logger;

    public ArticlesController(ILogger<HomeController> logger)
    {
        _logger = logger;
    }

    public IActionResult Index(string article)
    {
        // The cookie written won't be available until next page load anyway
        // so it doesn't matter that we get the cookies before we write to them
        // we still need to add the current article manually
        Dictionary<string, bool> read = GetArticleStatus();

        if (!string.IsNullOrWhiteSpace(article))
        {
            read["article_" + article] = true;
            SetArticleReadStatus(article, true);
        }

        foreach (var def in read) 
        {
            ViewData[def.Key] = def.Value;
        }
        return View();
    }

    public IActionResult Security(string article)
    {
        // The cookie written won't be available until next page load anyway
        // so it doesn't matter that we get the cookies before we write to them
        // we still need to add the current article manually
        Dictionary<string, bool> read = GetArticleStatus();

        if (!string.IsNullOrWhiteSpace(article))
        {
            read["article_" + article] = true;
            SetArticleReadStatus(article, true);
        }

        foreach (var def in read) 
        {
            ViewData[def.Key] = def.Value;
        }

        return View();
    }

    private void SetArticleReadStatus(string article, bool read)
    {
        CookieOptions option = new CookieOptions();
        option.Expires = DateTime.Now.AddDays(30);
 
        //Create a Cookie with a suitable Key and add the Cookie to Browser.
        Response.Cookies.Append("article_" + article, read.ToString());
    }

    private Dictionary<string, bool> GetArticleStatus()
    {
        Dictionary<string, bool> retVal = new();
        foreach (var cookie in HttpContext.Request.Cookies.Where(x => x.Key.StartsWith("article_")))
        {
            retVal[cookie.Key] = bool.Parse(cookie.Value);
        }
        return retVal;
    }


    [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
    public IActionResult Error()
    {
        return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
    }
}
