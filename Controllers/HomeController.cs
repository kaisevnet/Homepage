﻿using System.Xml;
using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using homepage.Models;

namespace homepage.Controllers;

public class HomeController : Controller
{
    private readonly ILogger<HomeController> _logger;

    public HomeController(ILogger<HomeController> logger)
    {
        _logger = logger;
    }

    public IActionResult Index()
    {
        return View();
    }

    [Route("Privacy")]
    public IActionResult Privacy()
    {
        return View();
    }
    [Route("Resources")]
    public IActionResult Resources()
    {
        return View();
    }
    [Route("sitemap")]
    [Route("sitemap.xml")]
    public IActionResult sitemap()
    {
        return View();
    }

    [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
    public IActionResult Error()
    {
        return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
    }
}
